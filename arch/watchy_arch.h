#ifndef WATCHY_ARCH_H
#define WATCHY_ARCH_H

#include <Watchy.h>
#include "arch.h"
#include "icons.h"

class WatchyArch : public Watchy{
    public:
        WatchyArch();
        void drawWatchFace();
        void drawTime();
        void drawDate();
        void drawBattery();
};

#endif