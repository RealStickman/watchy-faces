#include "watchy_html.h"
#include "FreeMonoBold8pt7b.h"
#include "FreeMonoBold10pt7b.h"
#include "FreeMonoBold12pt7b.h"
#include "FreeMonoBold15pt7b.h"

const uint8_t BATTERY_SEGMENT_WIDTH = 7;
const uint8_t BATTERY_SEGMENT_HEIGHT = 11;
const uint8_t BATTERY_SEGMENT_SPACING = 9;
const uint8_t IMAGE_WIDTH = 140;
const uint8_t IMAGE_HEIGHT = 140;

WatchyHtml::WatchyHtml(){} //constructor

void WatchyHtml::drawWatchFace(){
    // background image
    display.fillScreen(GxEPD_WHITE);
    // text color
    display.setTextColor(GxEPD_BLACK);
    display.setFont(&FreeMonoBold8pt7b);
    display.setCursor(0, 20);
    display.println("<html>");
    display.println("    <h1>");
    display.print("        ");
    drawTime();
    display.setFont(&FreeMonoBold8pt7b);
    display.println("    </h1>");
    display.print("    <h2>");
    String dayOfWeek = dayStr(currentTime.Wday);
    display.setFont(&FreeMonoBold10pt7b);
    display.print(dayOfWeek);
    display.setFont(&FreeMonoBold8pt7b);
    display.println("</h2>");
    display.print("    <p>");
    drawDate();
    display.println("</p>");
    display.println("</html>");
    drawBattery();
}

void WatchyHtml::drawTime(){
    display.setFont(&FreeMonoBold12pt7b);
    //display.setCursor(50, 170);
    if(currentTime.Hour < 10){
        display.print('0');
    }
    display.print(currentTime.Hour);
    display.print(':');
    if(currentTime.Minute < 10){
        display.print('0');
    }    
    display.print(currentTime.Minute);
    display.setFont(&FreeMonoBold8pt7b);
    display.println("");
}

void WatchyHtml::drawDate(){
    display.setFont(&FreeMonoBold8pt7b);

    int16_t  x1, y1;
    uint16_t w, h;

    //display.setCursor(50, 185);
    if(currentTime.Day < 10){
    display.print("0");      
    }
    display.print(currentTime.Day);
    display.print("/");

    if(currentTime.Month < 10){
    display.print("0");      
    }
    display.print(currentTime.Month);
    display.print("/");

    display.print(currentTime.Year + YEAR_OFFSET);// offset from 1970, since year is stored in uint8_t
}

void WatchyHtml::drawBattery(){
    display.drawBitmap(154, 173, battery, 37, 21, GxEPD_BLACK);
    display.fillRect(159, 178, 27, BATTERY_SEGMENT_HEIGHT, GxEPD_WHITE);//clear battery segments
    int8_t batteryLevel = 0;
    float VBAT = getBatteryVoltage();
    if(VBAT > 4.1){
        batteryLevel = 3;
    }
    else if(VBAT > 3.95 && VBAT <= 4.1){
        batteryLevel = 2;
    }
    else if(VBAT > 3.80 && VBAT <= 3.95){
        batteryLevel = 1;
    }    
    else if(VBAT <= 3.80){
        batteryLevel = 0;
    }

    for(int8_t batterySegments = 0; batterySegments < batteryLevel; batterySegments++){
        display.fillRect(159 + (batterySegments * BATTERY_SEGMENT_SPACING), 178, BATTERY_SEGMENT_WIDTH, BATTERY_SEGMENT_HEIGHT, GxEPD_BLACK);
    }
}