#ifndef WATCHY_HTML_H
#define WATCHY_HTML_H

#include <Watchy.h>
#include "icons.h"

class WatchyHtml : public Watchy{
    public:
        WatchyHtml();
        void drawWatchFace();
        void drawTime();
        void drawDate();
        void drawBattery();
};

#endif