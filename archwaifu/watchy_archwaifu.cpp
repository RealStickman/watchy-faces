#include "watchy_archwaifu.h"
#include "FreeMonoBold15pt7b.h"
#include "FreeMonoBold8pt7b.h"

const uint8_t BATTERY_SEGMENT_WIDTH = 7;
const uint8_t BATTERY_SEGMENT_HEIGHT = 11;
const uint8_t BATTERY_SEGMENT_SPACING = 9;
const uint16_t IMAGE_WIDTH = 110;
const uint16_t IMAGE_HEIGHT = 200;

WatchyArchwaifu::WatchyArchwaifu(){} //constructor

void WatchyArchwaifu::drawWatchFace(){
    // background image
    display.fillScreen(GxEPD_WHITE);
    // draw image
    display.drawBitmap(0, 0, archwaifu, IMAGE_WIDTH, IMAGE_HEIGHT, GxEPD_BLACK);
    // text color
    display.setTextColor(GxEPD_BLACK);
    drawTime();
    drawDate();
    drawBattery();
}

void WatchyArchwaifu::drawTime(){
    display.setFont(&FreeMonoBold15pt7b);
    display.setCursor(105, 70);
    if(currentTime.Hour < 10){
        display.print('0');
    }
    display.print(currentTime.Hour);
    display.print(':');
    if(currentTime.Minute < 10){
        display.print('0');
    }    
    display.print(currentTime.Minute);
}

void WatchyArchwaifu::drawDate(){
    display.setFont(&FreeMonoBold8pt7b);

    int16_t  x1, y1;
    uint16_t w, h;

    display.setCursor(105, 85);
    if(currentTime.Day < 10){
    display.print("0");      
    }
    display.print(currentTime.Day);
    display.print("/");

    if(currentTime.Month < 10){
    display.print("0");      
    }
    display.print(currentTime.Month);
    display.print("/");

    display.println(currentTime.Year + YEAR_OFFSET);// offset from 1970, since year is stored in uint8_t
}

void WatchyArchwaifu::drawBattery(){
    display.drawBitmap(154, 173, battery, 37, 21, GxEPD_BLACK);
    display.fillRect(159, 178, 27, BATTERY_SEGMENT_HEIGHT, GxEPD_WHITE);//clear battery segments
    int8_t batteryLevel = 0;
    float VBAT = getBatteryVoltage();
    if(VBAT > 4.1){
        batteryLevel = 3;
    }
    else if(VBAT > 3.95 && VBAT <= 4.1){
        batteryLevel = 2;
    }
    else if(VBAT > 3.80 && VBAT <= 3.95){
        batteryLevel = 1;
    }    
    else if(VBAT <= 3.80){
        batteryLevel = 0;
    }

    for(int8_t batterySegments = 0; batterySegments < batteryLevel; batterySegments++){
        display.fillRect(159 + (batterySegments * BATTERY_SEGMENT_SPACING), 178, BATTERY_SEGMENT_WIDTH, BATTERY_SEGMENT_HEIGHT, GxEPD_BLACK);
    }
}