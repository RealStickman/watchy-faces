#ifndef WATCHY_ARCHWAIFU_H
#define WATCHY_ARCHWAIFU_H

#include <Watchy.h>
#include "archwaifu.h"
#include "icons.h"

class WatchyArchwaifu : public Watchy{
    public:
        WatchyArchwaifu();
        void drawWatchFace();
        void drawTime();
        void drawDate();
        void drawBattery();
};

#endif